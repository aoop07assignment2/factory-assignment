package Ball;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import org.junit.Test;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import static org.junit.Assert.*;

/**
 * Created by Freddie on 31/05/2016.
 */
public class BallTest {
    @Test
    public void lockBall() throws Exception {

    }

    @Test
    public void unlockBall() throws Exception {

    }

    @Test
    public void tryBallLock() throws Exception {

    }

    @Test
    public void getState() throws Exception {

    }

    @Test
    public void getColour() throws Exception {

    }

    @Test
    public void getMaterial() throws Exception {

    }

    @Test
    public void getVelocity() throws Exception {

    }

    @Test
    public void getPosition() throws Exception {

    }

    @Test
    public void setState() throws Exception {

    }

    @Test
    public void setColour() throws Exception {

    }

    @Test
    public void setMaterial() throws Exception {

    }

    @Test
    public void setVelocity() throws Exception {

    }

    @Test
    public void setPosition() throws Exception {

    }

    @Test
    public void setMoneyValue() throws Exception {

    }

    @Test
    public void move() throws Exception {
        Ball instance = new Ball(
                Material.GLASS,
                State.GLASS,
                new Vector(),
                Constants.GLASS_COST);
        double x = instance.getPosition().getX();
        double y = instance.getPosition().getY();
        instance.move(new Rectangle(200, 200));
        assertTrue(instance.getPosition().getX() == x + 1
                && instance.getPosition().getY() == y + 1);
    }

    @Test
    public void getShape() throws Exception {

    }

    @Test
    public void moveTo() throws Exception {

    }

    @Test
    public void chechMachines() throws Exception {

    }

    @Test
    public void collision() throws Exception {

    }

    @Test
    public void isTouching() throws Exception {

    }

    @Test
    public void rebound() throws Exception {

    }

}