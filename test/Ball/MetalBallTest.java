package Ball;

import org.junit.Test;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

import static org.junit.Assert.*;

/**
 * Created by Freddie on 31/05/2016.
 */
public class MetalBallTest {

    @Test
    public void polish() throws Exception {
        MetalBall instance = new MetalBall(new Vector());
        assertTrue(instance.getColour() == Constants.DIRTY_COLOUR);
        instance.polish();
        assertTrue(instance.getColour() == Constants.METAL_COLOUR);
        instance.polish();
        assertTrue(instance.getColour() == Constants.METAL_COLOUR);
    }

    @Test
    public void crush() throws Exception {
        MetalBall instance = new MetalBall(new Vector());
        assertTrue(instance.getState() == State.METAL);
        instance.crush();
        assertTrue(instance.getState() == State.INGOT);
        instance.crush();
        assertTrue(instance.getState() == State.INGOT);
    }

    @Test
    public void getMoneyValue() throws Exception {
        MetalBall instance = new MetalBall(new Vector());
        assertTrue(instance.getMoneyValue() == 0);
        instance.crush();
        assertTrue(instance.getMoneyValue() == Constants.METAL_COST);
    }

}