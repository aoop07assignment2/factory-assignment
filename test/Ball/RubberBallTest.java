package Ball;

import org.junit.Test;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

import static org.junit.Assert.*;

/**
 * Created by Freddie on 31/05/2016.
 */
public class RubberBallTest {

    @Test
    public void inflate() throws Exception {
        Ball instance = new RubberBall(new Vector());
        assertTrue(instance.getState() == State.RUBBER);
        assertTrue(instance.getShape().getWidth() == 15
                && instance.getShape().getHeight() == 15);

        instance.inflate();
        assertTrue(instance.getShape().getWidth() == 15
                && instance.getShape().getHeight() == 15);

    }

    @Test
    public void getMoneyValue() throws Exception {

    }

}