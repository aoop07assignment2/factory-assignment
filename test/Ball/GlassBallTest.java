package Ball;

import org.junit.Test;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

import static org.junit.Assert.*;

/**
 * Created by Freddie on 31/05/2016.
 */
public class GlassBallTest {

    @Test
    public void polish() throws Exception {
        GlassBall instance = new GlassBall(new Vector());
        assertTrue(instance.getColour() == Constants.DIRTY_COLOUR);
        instance.polish();
        assertTrue(instance.getColour() == Constants.GLASS_COLOUR);
        instance.polish();
        assertTrue(instance.getColour() == Constants.GLASS_COLOUR);
    }

    @Test
    public void crush() throws Exception {
        GlassBall instance = new GlassBall(new Vector());
        assertTrue(instance.getState() == State.GLASS);
        instance.crush();
        assertTrue(instance.getState() == State.CRUSHED);
        instance.setState(State.HELIUM); // testing with obscure state that should never occur
        instance.crush();
        assertTrue(instance.getState() == State.CRUSHED);
    }

    @Test
    public void getMoneyValue() throws Exception {
        GlassBall instance = new GlassBall(new Vector());
        assertTrue(instance.getMoneyValue() == 0);
        instance.crush();
        assertTrue(instance.getMoneyValue() == Constants.GLASS_COST);
    }

}