/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007.Machine;

import Ball.Ball;
import Ball.GlassBall;
import Machine.Machine;
import Ball.Material;
import Ball.MetalBall;
import Ball.RubberBall;
import Ball.State;
import Machine.HeliumPump;
import Machine.HydraulicPress;
import Machine.Polisher;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javafx.geometry.Rectangle2D;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import recyclingfactorya007.Constants;
import recyclingfactorya007.RecyclingFactory;
import recyclingfactorya007.Vector;

/**
 *
 * @author Sarah
 */
public class MachineTest {
    
    private static Ball aRubberBall;
    private static Ball aMetalBall;
    private static Ball aGlassBall;
    private static Ball aBall;   
    
    private static Machine heliumPump;
    private static Machine hydraulicPress;
    private static Machine polisher;
    
    
    public MachineTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        aRubberBall = new RubberBall(new Vector());
        aMetalBall = new MetalBall(new Vector());
        aGlassBall = new GlassBall(new Vector());
        aBall = new Ball(Material.GLASS, State.RUBBER, new Vector(), 1);
        heliumPump = new HeliumPump();
        hydraulicPress = new HydraulicPress();
        polisher = new Polisher();
    }
    
    @AfterClass
    public static void tearDownClass() {
        Ball aRubberBall = null;
        Ball aMetalBall = null;
        Ball aGlassBall = null;
        Ball aBall = null;
        
        heliumPump = null;
        hydraulicPress = null;
        polisher = null;
        
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setValidMaterial method, of class Machine.
     */
    @Test
    public void testSetValidMaterials() {
        System.out.println("setValidMaterials");        
        Machine thisMachine = new Machine(new Rectangle2D(0,0,0,0));        
      
        ArrayList<Enum> materials = new ArrayList<>();        
        thisMachine.setValidMaterials(materials);
        Assert.assertEquals("Null materials", materials, thisMachine.getValidMaterials());


        materials = new ArrayList<>();
        materials.add(State.CRUSHED); //These are States(Enums), but not materials
        materials.add(State.HELIUM);
        thisMachine.setValidMaterials(materials);
        ArrayList<Enum> expectedArray = new ArrayList<>();
        Assert.assertEquals("States instead of materials materials", expectedArray, thisMachine.getValidMaterials());

        materials = new ArrayList<>();
        materials.add(Material.GLASS); //These are Materials - which is the intended Enum
        materials.add(Material.RUBBER);        
        thisMachine.setValidMaterials(materials);
        Assert.assertEquals("Valid Materials", materials, thisMachine.getValidMaterials());     
        
    }

    /**
     * Test of isMaterialValid method, of class Machine.
     */
    @Test
    public void testIsMaterialValid() {
        System.out.println("isMaterialValid");
        Machine thisMachine = new Machine(new Rectangle2D(0,0,0,0));        
        ArrayList<Enum> aMaterialArray = new ArrayList<>();
        aMaterialArray.add(Material.GLASS);
        aMaterialArray.add(Material.RUBBER);        
        thisMachine.setValidMaterials(aMaterialArray);
        Ball aNullBall = null;
        Boolean result;
        result = thisMachine.isMaterialValid(aRubberBall, aMaterialArray);
        Assert.assertTrue(result);
        result = thisMachine.isMaterialValid(aMetalBall, aMaterialArray);
        Assert.assertFalse(result);
        result = thisMachine.isMaterialValid(aGlassBall, aMaterialArray);
        Assert.assertTrue(result);
        result = thisMachine.isMaterialValid(aNullBall, aMaterialArray);
        Assert.assertFalse(result);

    }

    /**
     * Test of isBallTouching method, of class Machine.
     */
    @Test
    public void testIsBallTouching() {
        System.out.println("isBallTouching");
        Assert.assertFalse(hydraulicPress.isBallTouching());
        Ball collisionBall = new RubberBall(new Vector());
        Point2D touchingPoint = new Point2D.Double(Constants.HYDRALIC_PRESS_RECT.getMinX(), Constants.HYDRALIC_PRESS_RECT.getMinY());
        Point2D notTouchingPoint = new Point2D.Double(0,0);
        collisionBall.moveTo((Point2D.Double)touchingPoint);
        RecyclingFactory.addBall(collisionBall);
        Assert.assertTrue(hydraulicPress.isBallTouching());        
        collisionBall.moveTo((Point2D.Double)notTouchingPoint);        
        Assert.assertFalse(hydraulicPress.isBallTouching());    
          
    }

    /**
     * Test of interact method, of class Machine.
     */
    @Test
    public void testInteract() {
        //Helium Pump Machine call method
        Machine thisMachine = new Machine(new Rectangle2D(0,0,0,0));  
        //((HeliumPump)thisMachine).interact(aRubberBall); //this does nothing
        //((Machine)heliumPump).interact(aRubberBall);        
        Assert.assertEquals("Rubber Ball inflated", State.HELIUM, (State)aRubberBall.getState());
        Assert.assertEquals("Rubber Ball inflated notpolished", Constants.DIRTY_COLOUR, aBall.getColour());
        //HydraulidPrecc Machine call method
        ((Machine)hydraulicPress).interact(aMetalBall);
        Assert.assertNotEquals("metal ball crushed not inflated", State.HELIUM, (State)aMetalBall.getState());
        Assert.assertEquals("metal ball crushed", State.CRUSHED, (State)aMetalBall.getState());
        Assert.assertEquals("metal ball crushed not polished", Constants.DIRTY_COLOUR, aMetalBall.getColour());
        //Poslisher Machine call method
        ((Machine)polisher).interact(aBall);
        Assert.assertEquals("Werid test ball polished not inflated", State.RUBBER, (State)aBall.getState());
        Assert.assertNotEquals("Werid test ball polished not crushed", State.CRUSHED, (State)aBall.getState());
        Assert.assertEquals("Werid test ball polished", Constants.GLASS_COLOUR, aBall.getColour());
        
        try{
            ((Machine)polisher).interact(null);
        }catch(NullPointerException e){
            fail("Null exception");
        }
    }
    
}
