/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007.Machine;

import Ball.Ball;
import Ball.GlassBall;
import Ball.Material;
import Ball.MetalBall;
import Ball.RubberBall;
import Ball.State;
import Machine.HeliumPump;
import Machine.Polisher;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import recyclingfactorya007.Vector;
import recyclingfactorya007.Constants;

/**
 *
 * @author Sarah
 */
public class PolisherTest {
    
        
    private static Ball aRubberBall;
    private static Ball aMetalBall;
    private static Ball aGlassBall;
    private static Ball aBall;
    
    public PolisherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        aRubberBall = new RubberBall(new Vector());
        aMetalBall = new MetalBall(new Vector());
        aGlassBall = new GlassBall(new Vector());
        //Create a ball that is just plain wrong
        aBall = new Ball(Material.GLASS, State.INGOT, new Vector(), -9);
    }
    
    @AfterClass
    public static void tearDownClass() {
        Ball aRubberBall = null;
        Ball aMetalBall = null;
        Ball aGlassBall = null;
        Ball aBall = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of polish method, of class Polisher.
     */
    @Test
    public void testPolish() {
        System.out.println("polish");
        Polisher thisPolisher = new Polisher();
        thisPolisher.polish(aRubberBall);             
        Assert.assertEquals("Rubber ball polished", Constants.RUBBER_COLOUR, aRubberBall.getColour());
        
        thisPolisher.polish(aRubberBall);  
        Assert.assertEquals("Rubber ball that is already polished", Constants.RUBBER_COLOUR, aRubberBall.getColour());
        
        thisPolisher.polish(aMetalBall);
        Assert.assertEquals("Metal ball polished", Constants.METAL_COLOUR, aMetalBall.getColour());
        
        thisPolisher.polish(aGlassBall);
        Assert.assertEquals("Glass ball polished", Constants.GLASS_COLOUR, aGlassBall.getColour());
        
        thisPolisher.polish(aBall);
        Assert.assertEquals("Werid INGOT glass ball polished", Constants.GLASS_COLOUR, aBall.getColour());
        
        try{
            thisPolisher.polish(null);
        }catch(NullPointerException e){
            fail("Null exception");
        }
    }

    /**
     * Test of interact method, of class Polisher.
     */
    @Test
    public void testInteract() {
        System.out.println("Interact test inside 'HeliumPump'");       
        Polisher thisPolisher = new Polisher();
        
        thisPolisher.interact(aRubberBall);
        Assert.assertEquals("Rubber ball interacted with", Constants.RUBBER_COLOUR, aRubberBall.getColour());
        
        thisPolisher.interact(aMetalBall);
        Assert.assertEquals("Metal ball interacted with", Constants.METAL_COLOUR, aMetalBall.getColour());
        
        thisPolisher.interact(aGlassBall);
        Assert.assertEquals("Glass ball interacted with", Constants.GLASS_COLOUR, aGlassBall.getColour());
        
        try{
            thisPolisher.interact(null);
        }catch(NullPointerException e){
            fail("Null exception");
        }

    }
}
