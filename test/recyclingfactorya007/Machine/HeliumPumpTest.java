/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007.Machine;

import Ball.Ball;
import Ball.GlassBall;
import Ball.Material;
import Ball.MetalBall;
import Ball.RubberBall;
import Ball.State;
import Machine.HeliumPump;
import Machine.HeliumPump;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import recyclingfactorya007.Vector;

/**
 *
 * @author Sarah
 */
public class HeliumPumpTest {
    
    private static Ball aRubberBall;
    private static Ball aMetalBall;
    private static Ball aGlassBall;
    private static Ball aBall;
    
    public HeliumPumpTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        aRubberBall = new RubberBall(new Vector());
        aMetalBall = new MetalBall(new Vector());
        aGlassBall = new GlassBall(new Vector());
        //Create a ball that is just plain wrong
        aBall = new Ball(Material.GLASS, State.INGOT, new Vector(), -9);
    }
    
    @AfterClass
    public static void tearDownClass() {
        Ball aRubberBall = null;
        Ball aMetalBall = null;
        Ball aGlassBall = null;
        Ball aBall = null;
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of inflate method, of class HeliumPump.
     */
    @Test
    public void testInflate() {
        System.out.println("Inflate test inside 'HeliumPump'");
        
        HeliumPump thisHeliumPump = new HeliumPump();
        thisHeliumPump.inflate(aRubberBall);             
        Assert.assertEquals("Rubber ball inflated", State.HELIUM, (State)aRubberBall.getState());
        
        thisHeliumPump.inflate(aRubberBall);  
        Assert.assertEquals("Rubber ball that is already inflated", State.HELIUM, (State)aRubberBall.getState());
        
        thisHeliumPump.inflate(aMetalBall);
        Assert.assertNotEquals("Metal ball inflated", State.HELIUM, (State)aMetalBall.getState());
        
        thisHeliumPump.inflate(aGlassBall);
        Assert.assertNotEquals("Glass ball inflated", State.HELIUM, (State)aGlassBall.getState());
        
        thisHeliumPump.inflate(aBall);
        Assert.assertEquals("Werid INGOT glass ball inflated", State.INGOT, (State)aBall.getState());
        
        try{
            thisHeliumPump.inflate(null);
        }catch(NullPointerException e){
            fail("Null exception");
        }
    }

    /**
     * Test of interact method, of class HeliumPump.
     */
    @Test
    public void testInteract() {
        System.out.println("Interact test inside 'HeliumPump'");       
        HeliumPump thisHeliumPump = new HeliumPump();
        thisHeliumPump.interact(aRubberBall);
        Assert.assertEquals("Rubber ball interacted with", State.HELIUM, (State)aRubberBall.getState());
        thisHeliumPump.interact(aMetalBall);
        Assert.assertNotEquals("Metal ball interacted with", State.HELIUM, (State)aMetalBall.getState());
        thisHeliumPump.interact(aGlassBall);
        Assert.assertNotEquals("Glass ball interacted with", State.HELIUM, (State)aGlassBall.getState());
        try{
            thisHeliumPump.interact(null);
        }catch(NullPointerException e){
            fail("Null exception");
        }
        

    }
    
}
