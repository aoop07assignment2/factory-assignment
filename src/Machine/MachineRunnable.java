/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Machine;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Ball.Ball;
import static com.sun.org.apache.bcel.internal.Repository.instanceOf;
import recyclingfactorya007.Constants;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import recyclingfactorya007.RecyclingFactory;
/**
 *
 * @author Sarah
 */
public class MachineRunnable implements Runnable {
    
    private Machine machine;
    private ActionListener machineCheck;

    public MachineRunnable(Machine aMachine) {        
        machine = aMachine;
    }
    /**
     * @author Sarah
     * Getter for the Machine that this thread represents
     * @return Machine 
     */
    public Machine getMachine(){
        return machine;
    }
    /**
     * @author Sarah
     * While the factory is not paused, check to see if balls are touching a machine, and interact with them
     */
    public void run() {
        while (!RecyclingFactory.factoryPaused){
            machine.isBallTouching();
        }
    }   
}



