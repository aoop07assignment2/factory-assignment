/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Machine;

import java.util.ArrayList;
import Ball.Ball;
import Ball.Material;
import recyclingfactorya007.Constants;
import recyclingfactorya007.MachineInteraction;

/**
 *
 * @author Sarah
 */
public class Polisher extends Machine implements MachineInteraction {
    /**
     * Polisher Constructor is empty because the machines do not have a state
     */
    public Polisher(){
        super(Constants.POLISHER_RECT);
        ArrayList<Enum> validMaterials = new ArrayList<>();
        validMaterials.add(Material.GLASS);
        validMaterials.add(Material.METAL);   
        setValidMaterials (validMaterials);
    }
    /**
     * Polish balls that it is given
     * @param aBall - Ball object to be cleaned
     */
    public void polish(Ball aBall){
        if(aBall != null){
            aBall.colourLock.lock();
            aBall.materialLock.lock();
            try{
                aBall.polish();
            }
            finally{
                aBall.materialLock.unlock();
                aBall.colourLock.unlock();
            }
        }
    }
    /**
     * This machine only polishes the machine, 
     * Calls the interact method, and passes the Ball to polish
     * @param aBall 
     */
    @Override
    public void interact(Ball aBall) {
        polish( aBall);
    }
}
