    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Machine;

import java.util.ArrayList;
import Ball.Ball;
import Ball.Material;
import java.util.Iterator;
import javafx.geometry.Rectangle2D;
import recyclingfactorya007.Constants;
import recyclingfactorya007.MachineInteraction;
import recyclingfactorya007.RecyclingFactory;
import static recyclingfactorya007.RecyclingFactory.ballsLock;

/**
 *
 * @author Sarah
 */
public class Machine implements MachineInteraction{
    private ArrayList<Enum> validMaterials = new ArrayList<>();
    private Rectangle2D area;
    
    public Machine(Rectangle2D area){
        this.area = area;
    }
    /**
     * @author Sarah
     * Getter for the valid Material array
     * @return Array of Enums that represent the Materials valid to interact with a machine
     */
    public ArrayList<Enum> getValidMaterials(){
        return validMaterials;
    }
    /**
     * @author Sarah
     * Setter for the valid material array
     * @param materials Enum Array of materials that are valid to interact with a machine
     */
    public void setValidMaterials(ArrayList<Enum> materials){
        for(Enum anItem : materials){
            if(!(anItem instanceof Material)){
                validMaterials = new ArrayList<>();
                return;
            }
        }
        validMaterials = materials;

    }
    /**
     * @author Sarah
     * Getter for the area of the Machine that it is located
     * @return area that the Machine takes up in the factory
     */
    public Rectangle2D getArea(){
        return area;
    } 
    /* Helper Methods*/

    /**
     * @author Sarah
     * checks that the ball is one of the accepted materials
     * @param aBall Ball object to check if it is valid material
     * @param aMaterialArray An Array of Enum Material that is accepted by a machine
     * @return True if the ball's material is contained inside the array
     */
    public Boolean isMaterialValid(Ball aBall, ArrayList<Enum> aMaterialArray){
        if(aBall != null){  
            return aMaterialArray.stream().anyMatch((aMaterial) -> (aBall.getMaterial() == aMaterial)); 
        }
        return false;
    }
    /**
     * @author Sarah
     * Check the factory ball array and looks for all the balls touching the machine, 
     * Interact with all of them, and rebound them all.
     * @return True if one or more balls are touching the machine
     */
    public Boolean isBallTouching(){      
        Boolean testReturnStatment = false; /*This is for testing purposes to see the outcome of this method*/
        RecyclingFactory.ballsLock.lock();         
        try{
            for (Ball aBall : RecyclingFactory.getBalls()) {
                if(aBall != null){
                    if(aBall.tryBallLock()){
                        try{
                            if(area.contains(aBall.getPosition().getX(), aBall.getPosition().getY())){                          
                                interact(aBall);
                                aBall.rebound();
                                testReturnStatment = true;
                            }
                        }
                        finally{
                            /*If statement is needed for the packager - making balls null*/
                            if(aBall != null) aBall.unlockBall();
                        }
                    }
                }
            }
            return testReturnStatment;
        }
        finally{
            RecyclingFactory.ballsLock.unlock();
        }        
    }
    /**
     * @author Sarah
     * Each machine interacts in a different way, so call the correct interact method. 
     * @param aBall - Ball to interact with
     */
    public void interact(Ball aBall){
        if (this instanceof HeliumPump){
            ((HeliumPump) this).interact(aBall);
        }if (this instanceof HydraulicPress){
            ((HydraulicPress) this).interact(aBall);        
        }if (this instanceof Polisher){
            ((Polisher) this).interact(aBall);       
        }
    }    
}

