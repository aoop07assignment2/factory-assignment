/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Machine;

import java.util.ArrayList;
import Ball.Ball;
import recyclingfactorya007.Constants;
import Ball.Material;
import Ball.RubberBall;
import Ball.State;
import recyclingfactorya007.MachineInteraction;

/**
 *
 * @author Sarah
 */
public class HydraulicPress extends Machine implements MachineInteraction {
    /**
     * HydraulicPress Constructor is empty because the machines do not have a state
     */
    public HydraulicPress(){
        super(Constants.HYDRALIC_PRESS_RECT);
        ArrayList<Enum> validMaterials = new ArrayList<>();
        validMaterials.add(Material.GLASS);
        validMaterials.add(Material.METAL); 
        setValidMaterials (validMaterials);
    }
    /**
     * @author Sarah
     * @param aBall - Ball that needs to be crushed
     * Checks that the material of aBall is valid  
     */
    public void crush(Ball aBall){    
        if(aBall != null){
            aBall.materialLock.lock();
            aBall.stateLock.lock();
            try{            
                if(isMaterialValid(aBall, getValidMaterials())){
                    aBall.crush();
                }
            } finally{
                aBall.stateLock.unlock();
                aBall.materialLock.unlock();
            }
        }
    }
    
    @Override
    public void interact(Ball aBall) {
        crush(aBall);
    }
    
}
