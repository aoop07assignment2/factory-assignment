/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Machine;

import java.util.ArrayList;
import Ball.Ball;
import recyclingfactorya007.Constants;
import Ball.Material;
import Ball.RubberBall;
import Ball.State;
import java.awt.geom.Point2D;
import recyclingfactorya007.MachineInteraction;

/**
 *
 * @author Sarah
 */
public class HeliumPump extends Machine implements MachineInteraction {
    /**
     * HeliumPump Constructor is empty because the machines do not have a state
     */
    public HeliumPump(){   
        super(Constants.HELIUM_PUMP_RECT);
             
    }
    /**
     * @author Sarah
     * Ensures that the given ball is Rubber, and will change the size a bit to show it has been inflated some more.
     * And sets the state to Helium
     * @param aBall  - Ball to inflate
     */
    public void inflate(Ball aBall){  
        if(aBall != null){
            aBall.materialLock.lock();
            aBall.stateLock.lock();
            try{

                if (aBall.getMaterial() == Material.RUBBER){
                    aBall.setState(State.HELIUM);
                    aBall.changeSizeBy(5, 5);
                }
            }
            finally{
                aBall.stateLock.unlock();
                aBall.materialLock.unlock();
            }
        }
    }
    /**
     * This machine's job is only to inflate the balls
     * @param aBall Ball to interact with
     */
    @Override
    public void interact(Ball aBall) {
        inflate( aBall);
    }
    
}
