/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ball;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

/**
 *
 * @author Sarah
 */
public class MetalBall extends Ball{
    /**
     * @author Sarah
     * @param velocity direction and speed Vector
     */
    public MetalBall(Vector velocity){
        super(Material.METAL, State.METAL, velocity, Constants.METAL_COST);
    }

    /**
     * @author Freddie
     */
    public void polish(){
        setColour(Constants.METAL_COLOUR);
    }

    /**
     * @author Freddie
     */
    public void crush(){
        setState(State.INGOT);
    }
    /**
     * @author Sarah
     * This method currently has not been used in the factory
     * This method provides validation, if the ball is in the correct state to sell or not
     * @return value of the ball
     */
    public int getMoneyValue(){
        if (getState() == State.INGOT){
            return Constants.METAL_COST;
        }
        else{
            return 0;
        }
    }
}
