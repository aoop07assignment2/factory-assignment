/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ball;

import java.awt.Component;
import recyclingfactorya007.Constants;
import recyclingfactorya007.RecyclingFactory;

/**
 *
 * @author Sarah
 */
public class BallRunnable implements Runnable {
    
    private Ball ball;
    private Component component;

    /**
     * @author Sarah
     * @param aBall - The ball that is being looked after by this thread
     * @param aComponent - Graphics Engine
     */
    public BallRunnable(Ball aBall, Component aComponent) {
        ball = aBall;
        component = aComponent;
    }
    /**
     * @author Sarah
     * This method is basically for testing
     * @return the ball that is being looked after by this thread
     */
    public Ball getBall(){
        return ball;
    }
    /**
     * @author The book
     * Thread moves ball, repaints it and checks for collisions
     */
    public void run() {
        while (!RecyclingFactory.factoryPaused) {
            ball.move(component.getBounds());
            component.repaint();
            ball.collision();
            try {
                Thread.sleep(Constants.DELAY);
            } catch (InterruptedException e) {
            }
        }
    }
    
}
