/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ball;

/**
 *
 * @author Sarah
 */
/**
 * State of the balls:
 * Used Enum, for no reason other than to keep spelling consistent 
*/
public enum State { 
    RUBBER, METAL, GLASS, CRUSHED, HELIUM, INGOT 
};
