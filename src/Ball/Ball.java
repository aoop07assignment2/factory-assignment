/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ball;

import Machine.Machine;
import com.sun.javafx.geom.Rectangle;
import java.awt.Color;
import java.awt.geom.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import recyclingfactorya007.Constants;
import recyclingfactorya007.RecyclingFactory;
import recyclingfactorya007.Vector;
//import java.awt.geom;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

/**
 * 
 * @author Sarah
 */
public class Ball {
    private Color colour;
    private Enum material;
    private int moneyValue;
    private Point2D position;
    private Enum state;
    private Vector velocity;

    private int xSize = 15;
    private int ySize = 15;

    private double x = 0;
    private double y = 0;
    
    /**
     * @author Sarah
     * Locks
     */
    private ArrayList<Lock> ballLocks= new ArrayList<>();
    public Lock colourLock = new ReentrantLock();
    public Lock materialLock = new ReentrantLock();  
    public Lock moneyValueLock = new ReentrantLock();
    public ReentrantReadWriteLock positionLock = new ReentrantReadWriteLock();
    public Lock positionReadLock = positionLock.readLock();
    public Lock positionWriteLock = positionLock.writeLock();
    public Lock stateLock = new ReentrantLock(); 
    public Lock velocityLock = new ReentrantLock();
    public ReentrantReadWriteLock sizeLock = new ReentrantReadWriteLock(); 
    public Lock sizeReadLock = sizeLock.readLock();
    public Lock sizeWriteLock = sizeLock.writeLock();

    /**
     * @author Sarah
     * Constructor
     * @param material Enum for the type of material the ball is made out of
     * @param state Enum for the state the ball is currently in
     * @param velocity Vector to control the spawn direction and speed of a ball - handles gravity
     * @param moneyValue Currently not implemented, was for market value of ball in current state
     */
    public Ball(Enum material, Enum state, Vector velocity, int moneyValue){
        setColour(Constants.DIRTY_COLOUR);                
        setMaterial(material);
        setMoneyValue(moneyValue);
        setPosition(new Point2D.Double(0, 0));
        setState(state);
        setVelocity(velocity);
        /*Create an array of locks in alphabetical order*/
        ballLocks.add(colourLock);
        ballLocks.add(materialLock);
        ballLocks.add(moneyValueLock);
        ballLocks.add(positionWriteLock);
        ballLocks.add(sizeWriteLock);
        ballLocks.add(stateLock);
        ballLocks.add(velocityLock);

    }
    /*Locks*/
    /**
     * @author Sarah
     * Releases all the locks of the ball 
     * in reverse alphabetical order
     */
    public void unlockBall(){
        for(int lockIndex = ballLocks.size() - 1; lockIndex > -1; lockIndex--){
            try{
                ballLocks.get(lockIndex).unlock();
            }catch(IllegalMonitorStateException e){
                //Some times, we will unlock something, we dont own
            }
        }
    }

    /**
     * @author Sarah
     * Attempt to unlock all the ball's locks in alphabetical order.
     * If it reached the return statement, then no locks will be unlocked, 
     * How ever, if one if statement is false... 
     * then relase all the locks that have been taken
     * @return True if locked successful
     */
    public Boolean tryBallLock(){
        if(colourLock.tryLock()){
            if(materialLock.tryLock()){
                if(moneyValueLock.tryLock()){
                    if(positionWriteLock.tryLock()){
                        if(sizeWriteLock.tryLock()){
                            if(stateLock.tryLock()){
                                if(velocityLock.tryLock()){
                                    return true;
                                }
                                stateLock.unlock();
                            }
                            sizeWriteLock.unlock();
                        }
                        positionWriteLock.unlock();
                    }
                    moneyValueLock.unlock();
                }
                materialLock.unlock();
            }
            colourLock.unlock();
        }
        return false;
    }
    /* Getter and Setters */
    /**
     * @author Sarah
     * Allow the ball to grow or shrink by a given amount
     * @param xChange amount to grow or shrink on x
     * @param yChange amount to grow or shrink on y
     */
    public void changeSizeBy(int xChange, int yChange){
        sizeWriteLock.lock();
        try{
            xSize += xChange;
            ySize += yChange;        
        }
        finally{
            sizeWriteLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Getter for state variable - Enum
     * @return Enum - State (eg.  Heliumn, crushed)
     */
    public Enum getState() {        
        return state;
    }
    /**
     * @author Sarah
     * Getter for colour variable - Color
     * @return Color - colour of ball
     */
    public Color getColour() {        
        return colour;
    }
    /**
     * @author Sarah
     * Getter for the material variable - Enum
     * @return Enum Material 
     */
    public Enum getMaterial() {        
        return material;
    }
    /**
     * @author Sarah
     * Getter for velocity - Vector
     * @return Vector velocity
     */
    public Vector getVelocity() {        
        return velocity;
    }
    /**
     * @author Sarah
     * Getter for Position variable - Point2D
     * @return Point 2D 
     */
    public Point2D getPosition(){        
        return position;
    }
    /**
     * @author Sarah
     * Setter for state - Enum
     * @param aState - Enum state to set to
     */
    public void setState(Enum aState){        
        state = aState;        
    }
    /**
     * @author Sarah
     * Setter for Colour - Color
     * @param aColour - Color to change to
     */
    public void setColour(Color aColour){        
        colour = aColour;
    }
    /**
     * @author Sarah
     * Setter for Material - Enum
     * @param aMaterial  - Material to change to
     */
    public void setMaterial(Enum aMaterial){        
        material  = aMaterial;
    }
    /**
     * @author Sarah
     * Setter for Veloctiy - Vector
     * @param aVelocity - Velocity to change to
     */
    public void setVelocity(Vector aVelocity){        
        velocity  = aVelocity;
    }
    /**
     * @author Sarah
     * Setter for position - Point2D
     * @param aPosition - Position to move to
     */
    public void setPosition(Point2D.Double aPosition){        
        position  = aPosition;
    }
    /**
     * @author Sarah
     * Setter for moneyValue - int
     * @param aMoneyValue - value to change to
     */
    public void setMoneyValue(int aMoneyValue){        
        moneyValue = aMoneyValue;
    }
    
    /* Methods */

    /**
     * @author Sarah
     * Take a step within the screen
     * Lock and unlock of positionLock
     * Runs the collision method
     * @param bounds - The Rectange2D contraints of the window
     */
    public void move(Rectangle2D bounds){        
        positionReadLock.lock();
        velocityLock.lock();
        sizeReadLock.lock();
        try{            
            position.setLocation(
                position.getX()+ velocity.getXDirection(),
                position.getY() + velocity.getYDirection());
            if(position.getX() < bounds.getMinX()){
                position.setLocation(bounds.getMinX(), position.getY());
                velocity.bounce(Vector.wallDirection.RIGHT);
            }
            if(position.getX() + xSize >= bounds.getMaxX()){
                position.setLocation(bounds.getMaxX() - xSize, position.getY());
                velocity.bounce(Vector.wallDirection.LEFT);
            }
            if(position.getY() < bounds.getMinY()){
                position.setLocation(position.getX(), bounds.getMinY());
                velocity.bounce(Vector.wallDirection.UP);
            }
            if(position.getY() + ySize >= bounds.getMaxY()){
                position.setLocation(position.getX(), bounds.getMaxY() - ySize);
                velocity.bounce(Vector.wallDirection.DOWN);
            }  
            velocity.adjustVector(Constants.GRAVITY);
            collision();
        }

        finally{
            sizeReadLock.unlock();
            velocityLock.unlock();
            positionReadLock.unlock();
        }

    }
    /**
     * @author Sarah
     * Get the shape of the ball
     * If a ball is Metal and is an Ingot, it will return a square shape. (Round Rectange)
     * If a ball is Glass, and has been crushed, it will return a shattered ball shape (Arc)
     * If a ball is none of the above, it returns a circle shape 
     * This also handles the size of the shape
     * @return Ellispse2D of ball's shape and dimension
     */
    public RectangularShape getShape(){    
        positionReadLock.lock();
        materialLock.lock();
        stateLock.lock();
        sizeReadLock.lock();
        try{
            if (this.material == Material.METAL && this.state == State.INGOT){
                return new RoundRectangle2D.Float((float)position.getX(), (float)position.getY(), (float)xSize, (float)ySize, (float)1.5, (float)1.5); //float(position.getX()), float(position.getY()), xSize, ySize
            }
            if (this.material == Material.GLASS && this.state == State.CRUSHED){
                return new Arc2D.Double(new Rectangle2D.Double((int)position.getX(), (int)position.getY(), (int)xSize, (int)ySize), (double)120, (double)120, 2);
            }
            return new Ellipse2D.Double(position.getX(), position.getY(), xSize, ySize);            
        }
        finally{
            sizeReadLock.unlock();
            stateLock.unlock();
            materialLock.unlock();
            positionReadLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Move the ball to a place on the screen
     * Lock and unlock of positionLock
     * @param aPoint - new position to move to
     */
    public void moveTo(Point2D.Double aPoint){   
        positionWriteLock.lock();
        try{            
            position.setLocation(aPoint);
        }
        finally{
            positionWriteLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Check for a collision of a machine, 
     * if touching a machine, interact with it, and rebound
     */
    public void checkMachines(){
        RecyclingFactory.machineLock.lock();
        positionReadLock.lock();
        velocityLock.lock();
        try{
            for(Machine aMachine : RecyclingFactory.getMachines()){
                if(aMachine.getArea().contains(this.getPosition().getX(), this.getPosition().getY())){
                    aMachine.interact(this);
                    rebound();
                }
            }
        }finally{
            velocityLock.unlock();
            positionReadLock.unlock();
            RecyclingFactory.machineLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Collision checker:
     * Looks around at all the balls in the factory, it they are very close to each other, they will rebound
     * Lock and unlock of the whole ball (all locks)
     */
    public void collision(){        
        RecyclingFactory.ballsLock.lock();
        try{
            if(RecyclingFactory.getBalls().size() > 1){
                for(int ballIndex = 0; ballIndex <  RecyclingFactory.getBalls().size(); ballIndex ++){
                    this.positionReadLock.lock();
                    RecyclingFactory.getBalls().get(ballIndex).positionReadLock.lock();
                    try{
                        if(RecyclingFactory.getBalls().get(ballIndex) != null){
                            if (!this.equals(RecyclingFactory.getBalls().get(ballIndex)) &&
                                    (isTouching(RecyclingFactory.getBalls().get(ballIndex)))){
                                //if you are not comaparing yourself against yourself, and you are touching another ball
                                positionWriteLock.lock();
                                RecyclingFactory.getBalls().get(ballIndex).positionWriteLock.lock();
                                try{
                                    this.rebound();
                                }
                                finally{
                                    RecyclingFactory.getBalls().get(ballIndex).positionWriteLock.unlock();
                                    positionWriteLock.unlock();
                                }
                            }
                        }
                    }
                    finally{
                        RecyclingFactory.getBalls().get(ballIndex).positionReadLock.unlock();
                        positionReadLock.unlock();
                    }
                }
            }
        }finally{
            RecyclingFactory.ballsLock.unlock();
        }
        
    }        
    /**
     * @author sarah
     * Checks if a ball is touching another ball by comparing positions on the screen
     * @param anotherBall aBall to compare it's self against
     * Lock and unlock of the position of the ball
     * @return True if the balls are touching
     */
    public Boolean isTouching(Ball anotherBall){ 
        /*int raduis = 3;   
        positionReadLock.lock();
        try{            
            return((anotherBall.getPosition().getX() - raduis) < this.position.getX() &&
                    this.position.getX() < (anotherBall.getPosition().getX() + raduis) &&
                    (anotherBall.getPosition().getY() - raduis) < this.position.getY() &&
                    this.position.getY() < (anotherBall.getPosition().getY() + raduis));
        }
        finally{
            positionReadLock.unlock();
        }*/
        positionReadLock.lock();
        try{            
            return(anotherBall.getPosition().getX() == this.position.getX()  &&
                    anotherBall.getPosition().getY() == this.position.getY()
                    );
        }
        finally{
            positionReadLock.unlock();
        }
    }
    /**
     * @author Freddie
     * Changes the direction of a ball once collided with another ball
     */
    public void rebound(){
        velocity.rebound();
        //bump();
    }
    /**
     * @author Freddie
     */
    public void bump() {
        position.setLocation(position.getX() + velocity.getXDirection(), position.getY() + velocity.getYDirection());
    }


    /**
     *
     *  The following methods are here to be called from the machines
     *  but it is up to the specific ball type to handle them appropriately
     *
     */

    /**
     * @author Freddie
     * polish methods are for the subclasses to implements
     */
    public void polish(){
        // method for subclasses to implement
    }
    /**
     * @author Freddie
     */
    public void inflate(){
        // method for subclasses to implement
    }
    /**
     * @author Freddie
     */
    public void crush(){
        // method for subclasses to implement
    }
}
