/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ball;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

/**
 *
 * @author Sarah
 */
public class GlassBall extends Ball{
    /**
     * @author Sarah
     * @param velocity direction and speed
     */
    public GlassBall(Vector velocity){
        super(Material.GLASS, State.GLASS, velocity, Constants.GLASS_COST);
    }
    /**
     * @author Sarah
     */
    public void polish(){
        setColour(Constants.GLASS_COLOUR);
    }

    /**
     * @author Sarah
     */
    public void crush(){
        setState(State.CRUSHED);
    }

    /**
     * Sarah
     * This method currently has not been used in the factory
     * This method provides validation, if the ball is in the correct state to sell or not
     * @return value of the ball
     */
    public int getMoneyValue(){
        if (getState() == State.CRUSHED){
            return Constants.GLASS_COST;
        }
        else{
            return 0;
        }
    }

}
