/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ball;

import java.awt.geom.*;
import recyclingfactorya007.Constants;
import recyclingfactorya007.Vector;

/**
 *
 * @author Sarah
 */
public class RubberBall extends Ball{
    /**
     * Sarah
     * @param velocity - Vector to determine direction and gravity
     */
    public RubberBall(Vector velocity){
        super(Material.RUBBER, State.RUBBER, velocity, Constants.RUBBER_COST);
}

    /**
     * @author Sarah
     * Inflates the ball by setting the state to Helium
     */
    public void inflate(){
        setState(State.HELIUM);
    }
    /**
     * @author Sarah
     * This method currently has not been used in the factory
     * This method provides validation, if the ball is in the correct state to sell or not
     * @return value of the ball
     */
    public int getMoneyValue(){
        if (getState() == State.HELIUM){
            return Constants.RUBBER_COST;
        }
        else{
            return 0;
        }
    }
    /**
     * @author Sarah
     * Changes the colour of the ball to clean colour
     */
    public void polish() {
        setColour(Constants.RUBBER_COLOUR);
    }
}
