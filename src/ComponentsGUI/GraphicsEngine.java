/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComponentsGUI;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.JPanel;
import Ball.Ball;
import java.awt.Color;
import java.awt.Image;
import javax.swing.ImageIcon;
import recyclingfactorya007.Constants;

import recyclingfactorya007.RecyclingFactory;

/**
 *
 * @author Sarah
 */
public class GraphicsEngine extends JPanel{
    /**
     * @author Book + Sarah
     * This method is slightly abused - This draws the machines repeatedly
     * @param canvas Graphics
     */
    public void paintComponent(Graphics canvas){
        super.paintComponent(canvas);
        Graphics2D canvas2D = (Graphics2D) canvas;
        for(Ball b : RecyclingFactory.getBalls()){
            canvas2D.setColor(b.getColour());
            canvas2D.fill(b.getShape());
        }
        canvas.drawImage(Constants.IMAGE_HELIUM_PUMP, (int)Constants.IMAGE_HELIUM_PUMP_POS.getX(), (int)Constants.IMAGE_HELIUM_PUMP_POS.getY(),  Constants.IMAGE_WIDTH_HELIUM_PUMP, Constants.IMAGE_HEIGHT_HELIUM_PUMP, this);
        canvas.drawImage(Constants.IMAGE_POLISHER, (int)Constants.IMAGE_POLISHER_POS.getX(), (int)Constants.IMAGE_POLISHER_POS.getY(),  Constants.IMAGE_WIDTH_POLISHER, Constants.IMAGE_HEIGHT_POLISHER, this);
        canvas.drawImage(Constants.IMAGE_HYDROLIC_PRESS, (int)Constants.IMAGE_HYDROLIC_PRESS_POS.getX(), (int)Constants.IMAGE_HYDROLIC_PRESS_POS.getY(),  Constants.IMAGE_WIDTH_HYDROLIC_PRESS, Constants.IMAGE_HEIGHT_WHYDROLIC_PRESS, this);
    }
    /**
     * @author Book
     * @return Dimension of the width of the window 
     */
    public Dimension getPreferredSize(){
        return new Dimension(Constants.DEFAULT_WIDTH, Constants.DEFAULT_HEIGHT);
    }
}
