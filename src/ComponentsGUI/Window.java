/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * author Book, Sarah and Freddie
 */
package ComponentsGUI;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import Ball.Ball;
import Ball.BallRunnable;
import Ball.GlassBall;
import Ball.Material;
import Ball.MetalBall;
import Ball.RubberBall;
import Machine.HeliumPump;
import Machine.HydraulicPress;
import Machine.Machine;
import Machine.MachineRunnable;
import Machine.Polisher;
import java.util.Random;
import recyclingfactorya007.Constants;
import recyclingfactorya007.RecyclingFactory;
import recyclingfactorya007.Vector;

public class Window extends JFrame {
    /**
     * @author Book + Sarah + Freddie
     */
    private GraphicsEngine graphicsEngine;
    public Window() {
        graphicsEngine = new GraphicsEngine();
        add(graphicsEngine, BorderLayout.CENTER);
        
        addMachine(new HeliumPump());
        addMachine(new HydraulicPress());
        addMachine(new Polisher());
        /*
         *@author Sarah
         *give the balls a random direction to begin with
         */
        Random randomGenerator = new Random();
        int maxRange = 2;

        JPanel buttonPanel = new JPanel();
        addButton(buttonPanel, "Glass Ball", new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                addBall(new GlassBall(new Vector(randomGenerator.nextFloat(), randomGenerator.nextFloat())));
            }
        });
        /**
         * @author Sarah
         */
        addButton(buttonPanel, "Metal Ball", new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                addBall(new MetalBall(new Vector(randomGenerator.nextFloat(), randomGenerator.nextFloat())));
            }
        });
        /**
         * @author Sarah
         */
        addButton(buttonPanel, "Rubber Ball", new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                addBall(new RubberBall(new Vector(randomGenerator.nextFloat(), randomGenerator.nextFloat())));
            }
        });
        /**
         * @author Sarah
         */
        addButton(buttonPanel, "Close", new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
        /**
         * @author Freddie
         */
        addButton(buttonPanel, "Play/Pause", new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                RecyclingFactory.pause();
            }
        });
        /**
         * @author Freddie
         */
        addButton(buttonPanel, "Stop", new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                RecyclingFactory.stop();
            }
        });
        add(buttonPanel, BorderLayout.SOUTH);
        pack();
    }
    /**
     * @author Book
     * @param container - Section on the screen to add buttons to
     * @param title - Text of the button
     * @param listener - Action listener when button pressed 
     */
    public void addButton(Container container, String title, ActionListener listener) {
        JButton button = new JButton(title);
        container.add(button);
        button.addActionListener(listener);
    }
    /**
     * @author Sarah
     * Creates machine thread and adds machine to the factory
     * @param aMachine - Machine to add to factory 
     */
    public void addMachine(Machine aMachine) {
        RecyclingFactory.addMachine(aMachine);
        Runnable machineRunnable = new MachineRunnable(aMachine);
        Thread machineThread = new Thread(machineRunnable);
        RecyclingFactory.addMachineThread(machineThread);
        kickThread(machineThread);
    }
    /**
     * @author Sarah
     * Creates ball thread and adds ball to the factory
     * @param aBall - Ball to add to the factory
     */
    public void addBall(Ball aBall) {
        RecyclingFactory.addBall(aBall);
        Runnable ballRunnable = new BallRunnable(aBall, graphicsEngine);
        Thread ballThread = new Thread(ballRunnable);
        RecyclingFactory.addBallThread(ballThread);
        kickThread(ballThread);
        
    }
    /**
     * @author Freddie
     * @param aThread aThread to kick
     */
    public void kickThread(Thread aThread){
        aThread.start();
        if(RecyclingFactory.factoryPaused) {            
            aThread.suspend();
        }
    }

}
