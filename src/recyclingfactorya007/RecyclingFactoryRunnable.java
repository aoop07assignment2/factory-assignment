/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007;

import Ball.Ball;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * Not bring used
 * @author Sarah
 */
public class RecyclingFactoryRunnable implements Runnable{
    
    ActionListener spawnBall;
    Timer t = new Timer(100, spawnBall);
    
    public RecyclingFactoryRunnable(){
        class spawnBall implements ActionListener{
            public void actionPerformed(ActionEvent event){
                attemptToAddBall();                
            }         
        }   
        spawnBall = new spawnBall();
    }
    public void attemptToAddBall(){
        RecyclingFactory.waitingBallsLock.lock();
        try{
            if(RecyclingFactory.waitingBalls.size() > 0){
                RecyclingFactory.addBall(RecyclingFactory.waitingBalls.get(0));
                RecyclingFactory.waitingBalls.set(0, null);
            }
        }
        finally{
            RecyclingFactory.waitingBallsLock.unlock();
        }
    }
    public void run() {        
        t.start();   
    }
}
