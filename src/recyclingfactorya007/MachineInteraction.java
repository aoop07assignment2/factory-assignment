/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007;

import Ball.Ball;

/**
 *
 * @author Sarah
 * All classes implementing this interface must have a method called interact, that takes a ball and is void reutrn type
 */
public interface MachineInteraction {
    void interact(Ball aBall);
}
