/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007;
import Ball.Ball;
import java.util.ArrayList;
import Ball.GlassBall;
import Ball.Material;
import Ball.MetalBall;
import Ball.RubberBall;
import Ball.State;
import Machine.Machine;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ConcurrentModificationException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import static recyclingfactorya007.RecyclingFactory.machineLock;


/**
 * @author Sarah 
 */
public class RecyclingFactory{

    public static java.util.ArrayList<Ball> balls = new ArrayList<>();
    public static java.util.ArrayList<Thread> ballThreads = new ArrayList<>();
    public static boolean factoryPaused = false;
    public static java.util.ArrayList<Machine> machines = new ArrayList<>();
    public static java.util.ArrayList<Thread> machineThreads = new ArrayList<>();
    private static int profit = 0;        
    public static java.util.ArrayList<Ball> waitingBalls = new ArrayList<>();
    
    public static Lock ballsLock = new ReentrantLock();
    public static Lock ballThreadLock = new ReentrantLock();
    public static Lock factoryPausedLock = new ReentrantLock();
    public static Lock machineLock = new ReentrantLock();
    public static Lock machineThreadLock = new ReentrantLock();
    public static Lock profitLock = new ReentrantLock();
    public static Lock waitingBallsLock = new ReentrantLock();
    
    /**
     * @author Sarah
     * locks the factory in alphabetical order
     */
    public void lockFactory(){
        ballsLock.lock();
        ballThreadLock.lock();
        factoryPausedLock.lock();               
        machineLock.lock();
        machineThreadLock.lock();
        profitLock.lock();
        waitingBallsLock.lock();
    }
    /**
     * @author Sarah
     * Unlocks the factory in reverse alphabetical order
     */
    public void unlockFactory(){
        waitingBallsLock.lock();
        profitLock.lock();
        machineThreadLock.lock();
        machineLock.lock();
        factoryPausedLock.lock();  
        ballThreadLock.lock();
        ballsLock.lock();
    }
    /**
     * @author Sarah
     * Add a machine to the factory
     * @param aMachine A machine object
     */
    public static void addMachine(Machine aMachine){        
        machines.add(aMachine);
    }
    /**
     * @author Sarah
     * Add a machine thread to the factory
     * @param thread - Contains information about machine thread
     */
    public static void addMachineThread(Thread thread){
        machineLock.lock();
        try{
            machineThreads.add(thread);
        }
        finally{
            machineLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Getter for Machine threads array
     * @return machine Thread array
     */
    public static ArrayList<Thread> getMachineThreads() {       
        return machineThreads;
    }
    /**
     * @author Sarah
     * Getter for Machine objects array
     * @return machine object array
     */
    public static ArrayList<Machine> getMachines() {  
        machineLock.lock();
        try{
            return machines;
        }
        finally{
            ballsLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Add a ball to the factory
     * @param aBall a ball that the user choose.
     */
    public static void addBall(Ball aBall){
        ballsLock.lock();
        try{
            balls.add(aBall);
        }
        finally{
            ballsLock.unlock();
        }
    }   
    /**
     * @author Sarah
     * Add a ball thread to the factory ball thread array
     * @param thread a ball thread
     */
    public static void addBallThread(Thread thread){        
       ballThreads.add(thread);
    }
    /**
     * @author Sarah
     * Getter for the ball thread array
     * @return Ball thread array
     */
    public static ArrayList<Thread> getBallThreads() {
        return ballThreads;

    }
    /**
     * @author Sarah
     * Getter for the ball object array
     * @return ball object array
     */
    public static ArrayList<Ball> getBalls() {     
        ballsLock.lock();
        try{
            return balls;
        }
        finally{
            ballsLock.unlock();
        }
    }
    /**
     * @author Sarah
     * Takes a value to add to the total profit of the factory
     * @param aMoneyValue int value of the ball being sold
     */
    private void addProfit(int aMoneyValue){        
        this.profit += aMoneyValue;
    }
    /**
     * @author Sarah
     * Clear all arrays set, reset all attributes
     */
    public static void reset(){
        factoryPaused = false;
        profit = 0;
        machines = new ArrayList<>();
        machineThreads = new ArrayList<>();
        waitingBalls = new ArrayList<>();
        balls = new ArrayList<>();
        ballThreads = new ArrayList<>();        
    }
    /**
     * @author Sarah
     * Resets the factory
     */
    public static void stop(){
        reset();
    }

    /**
     * @author Freddie
     */
    public static void pause() {        
        try{
            factoryPausedLock.lock();
            ballThreadLock.lock();
            RecyclingFactory.factoryPaused = !RecyclingFactory.factoryPaused;
            if (RecyclingFactory.factoryPaused) {
                for (Thread aThread : RecyclingFactory.getBallThreads()) {
                    aThread.suspend();
                }
            } else {
                for (Thread aThread : RecyclingFactory.getBallThreads()) {
                    aThread.resume();
                }
            }
        }finally{
            ballThreadLock.unlock();
            factoryPausedLock.unlock();
        }
        
    }
    
}
