/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007;

/**
 * @author Freddie
 */
public class Vector {
    private float xDirection;
    private float yDirection;



    /**
     * @author Freddie
     */
    public Vector( float xDirection, float yDirection ) {
        this.xDirection = xDirection;
        this.yDirection = yDirection;
    }


    /**
     * @author Freddie
     */
    public Vector() {
        this.xDirection = 1;
        this.yDirection = 1;
    }


    /**
     * @author Freddie
     * @return xDirection
     */
    public float getXDirection() {
        return xDirection;
    }


    /**
     * @author Freddie
     * @return yDirection
     */
    public float getYDirection() {
        return yDirection;
    }


    /**
     * @author Freddie
     * @param newXDirection
     */
    public void setXDirection(float newXDirection) {
        xDirection = newXDirection;
    }


    /**
     * @author Freddie
     * @param newYDirection
     */
    public void setYDirection(float newYDirection) {
        xDirection = newYDirection;
    }


    /**
     * @author Freddie
     * @param toAdd
     */
    public void addVector(Vector toAdd){
        xDirection += toAdd.getXDirection();
        yDirection += toAdd.getYDirection();
    }


    /**
     * @author Freddie
     * @param aWallDirection
     */
    public void bounce(wallDirection aWallDirection) {
        switch (aWallDirection) {
            case LEFT:
            case RIGHT:
                xDirection = -xDirection;
                break;
            case UP:
            case DOWN:
                yDirection = -yDirection;
                break;
        }
    }


    /**
     *
     * @author Freddie
     */
    public void rebound() {
        xDirection = -xDirection;
        yDirection = -yDirection;
    }

    /**
     * @author Freddie
     * @param gravity
     */
    public void adjustVector(float gravity) {
        adjustForGravity(gravity);
    }

    /**
     * @author Freddie
     * @param gravity
     */
    public void adjustForGravity(float gravity) {
        yDirection += gravity / (1000 / Constants.DELAY);
        decelerateForResistance();
    }

    public void decelerateForResistance() {
        yDirection = yDirection * 0.999f;
        xDirection = xDirection * 0.999f;
    }

    /**
     * @author Freddie
     */
    public enum wallDirection {LEFT, RIGHT, UP, DOWN}
}
