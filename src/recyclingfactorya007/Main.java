/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recyclingfactorya007;

import ComponentsGUI.GraphicsEngine;
import ComponentsGUI.Window;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import Ball.Ball;

/**
 * @author The Book
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new Window();
                frame.setTitle("Freddie and Sarah's Recycling Factory");
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);              
            }
        });        
    }
}


