package recyclingfactorya007;

import java.awt.Color;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.net.URL;
import javafx.geometry.Rectangle2D;
import static javafx.scene.input.DataFormat.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 * @author Sarah and Freddie
 * Created by Freddie on 10/05/2016.
 * Edited by Sarah
 */
public class Constants {
    /*Positions of machines*/   
    
    public static final Point2D IMAGE_HELIUM_PUMP_POS = new Point2D.Double(200, 250);
    public static final Image IMAGE_HELIUM_PUMP = new ImageIcon(Constants.class.getResource("/images/heliumPump.png")).getImage();
    public static final int IMAGE_WIDTH_HELIUM_PUMP = 20;
    public static final int IMAGE_HEIGHT_HELIUM_PUMP = 37;
    public static final Rectangle2D HELIUM_PUMP_RECT = new Rectangle2D(IMAGE_HELIUM_PUMP_POS.getX(),IMAGE_HELIUM_PUMP_POS.getY(),IMAGE_WIDTH_HELIUM_PUMP,IMAGE_HEIGHT_HELIUM_PUMP);
    
    public static final Point2D IMAGE_POLISHER_POS = new Point2D.Double(200, 180);
    public static final Image IMAGE_POLISHER = new ImageIcon(Constants.class.getResource("/images/polisher.jpg")).getImage();
    public static final int IMAGE_WIDTH_POLISHER = 100;
    public static final int IMAGE_HEIGHT_POLISHER = 43;
    public static final Rectangle2D POLISHER_RECT = new Rectangle2D(IMAGE_POLISHER_POS.getX(),IMAGE_POLISHER_POS.getY(),IMAGE_WIDTH_POLISHER,IMAGE_HEIGHT_POLISHER);
    
    public static final Point2D IMAGE_HYDROLIC_PRESS_POS = new Point2D.Double(320, 165);
    public static final Image IMAGE_HYDROLIC_PRESS = new ImageIcon(Constants.class.getResource("/images/hydrolicPress.png")).getImage();
    public static final int IMAGE_WIDTH_HYDROLIC_PRESS = 50;
    public static final int IMAGE_HEIGHT_WHYDROLIC_PRESS = 50;     
    public static final Rectangle2D HYDRALIC_PRESS_RECT = new Rectangle2D(IMAGE_HYDROLIC_PRESS_POS.getX(),IMAGE_HYDROLIC_PRESS_POS.getY(),IMAGE_WIDTH_HYDROLIC_PRESS,IMAGE_HEIGHT_WHYDROLIC_PRESS);
    
    /* Sarah tested these position, and they are now accurate */
;
    
    public static final Point2D ENTRY_FACTORY_POS = new Point2D.Double(0,0);
    
    
    public static final Point2D EXIT_HELIUM_FACTORY_POS = new Point2D.Double(400,10);
    public static final Point2D EXIT_CRUSHED_GLASS_FACTORY_POS = new Point2D.Double(300,340);
    public static final Point2D EXIT_IGNOT_FACTORY_POS = new Point2D.Double(400,310);
    
    /* Costs of the matierals */
    public static final int METAL_COST = 50;
    public static final int RUBBER_COST = 10;
    public static final int GLASS_COST = 20;
    
    /* Colours of the materials*/
    public static final Color DIRTY_COLOUR = new Color(139, 69, 19);// Brown
    public static final Color METAL_COLOUR = Color.LIGHT_GRAY;//"#404040";
    public static final Color RUBBER_COLOUR = Color.ORANGE;//6666ff";
    public static final Color GLASS_COLOUR = Color.blue;//"#99e6ff";
    
    /* GUI Constants */
    public static final int DELAY = 5;
    public static final int DEFAULT_WIDTH = 450;
    public static final int DEFAULT_HEIGHT = 350;
    public static final Rectangle FACTORY_ARENA = new Rectangle(DEFAULT_WIDTH, DEFAULT_HEIGHT, 0, 0);


    public static final float GRAVITY = 5.3f;
}
